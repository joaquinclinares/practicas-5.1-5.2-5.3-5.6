<?php
class Contacte{
    private static $contador=1000;
    private $id;
    private $nom;
    private $telefon;

    public function __construct($datos){
        self::incrementaContador();
        $this->nom=$datos['nombre'];
        $this->telefon=$datos['telefono'];
        $this->id=self::$contador;
    }
    function __clone(){
        self::incrementaContador();
        $this->id=self::$contador;
    }
    public function __toString(){
        return $this->nom.'( '.$this->telefon.' )';
    }
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
        return $this;
    }
    public function getNom(){
        return $this->nom;
    }
    public function setNom($nom){
        $this->nom = $nom;
        return $this;
    }
    public function getTelefon(){
        return $this->telefon;
    }
    public function setTelefon($telefon){
        $this->telefon = $telefon;

     return $this;
    }
    public static function incrementaContador(){
        self::$contador+=1;
    }
}
?>