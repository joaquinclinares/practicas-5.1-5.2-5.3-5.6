<?php
class PContacte extends Contacte{
    private $direccion;
    private $ciudad;
    private $provincia;

    public function __construct($datos){
        parent::__construct($datos);
        $this->direccion=$datos['direccion'];
        $this->ciudad=$datos['ciudad'];
        $this->provincia=$datos['provincia'];
    }
    public function __toString(){
        return parent::__toString().'</br>'.$this->direccion.', '. $this->ciudad.', '.$this->provincia;
    }
    public function __clone(){ 
    }
    public function getDireccion(){
        return $this->direccion;
    }
    public function setDireccion($direccion){
        $this->direccion = $direccion;
        return $this;
    }
    public function getCiudad(){
        return $this->ciudad;
    }
    public function setCiudad($ciudad){
        $this->ciudad = $ciudad;
        return $this;
    }
    public function getProvincia(){
        return $this->provincia;
    }
    public function setProvincia($provincia){
        $this->provincia = $provincia;
        return $this;
    }
}

?>