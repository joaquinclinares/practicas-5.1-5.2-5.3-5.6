<?php 
    require_once('Contacte.class.php');
    require_once('Agenda.class.php');

    $auxAgenda=new Agenda();

    $contacto1=new Contacte('Fernando',965889654);
    $contacto2=new Contacte('Luis',965847822);
    $contacto3=new Contacte('Alba',965888842);

    $auxAgenda->añadeContacto($contacto1);
    $auxAgenda->añadeContacto($contacto2);
    $auxAgenda->añadeContacto($contacto3);

    $auxAgenda->_toString();

    $auxCopia=clone($auxAgenda);
    $auxCopia->borraContactoPosicion(0);
    $auxCopia->getContactos()[1]->setNom('Freddie');

    echo "</br>";
    $auxCopia->_toString();
    echo "</br>";
    $auxAgenda->_toString();
?>