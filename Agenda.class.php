<?php

require_once('Contacte.class.php');

class Agenda{

    private $contactos=array();
    function añadeContacto($contacto){
        array_push($this->contactos,$contacto);
    }
    public function __construct(){
       
    }   

    function borraContactoId($identif){
        $contador=0;
        foreach($this->contactos as $contac){
            if($contac->getId()==$identif){
                unset($this->contactos[$contador]);
                $this->contactos=array_values($this->contactos);
            }
        $contador++;
           }
    }
    
    function borraContactoPosicion($posicion){
        unset($this->contactos[$posicion]);
        $this->contactos=array_values($this->contactos);
    }
    public function __toString(){
        $contador=0;
        foreach($this->contactos as $con){
            echo $con;
            echo '<br>';
            $contador++;
        }
    }
    public function muestraLista(){
        echo "<ul>";
        foreach($this->contactos as $posicion){
            $url='infocontacte.php?nombre='.$posicion->getNom().'&telefono='.$posicion->getTelefon();
            echo'<li>'.$posicion->getNom().'---->'.'<a href="'.$url.'">Ver contacto</a>'.'</li>';
    }
        echo "</ul>"; 
    }
    public function getContactos(){
        return $this->contactos;
    }
    public function setContactos($contactos){
        $this->contactos = $contactos;
        return $this;
    }
    public function __clone(){
        $copia=array();
        foreach ($this->contactos as $indice => $propieds) {
            $copia[$indice]=clone($propieds);
            $copia[$indice]=$propieds;
        }
        //$copia = array_merge(array(),$this->contactos);
        //$this->contactos=$copia;
    }
}
 ?>