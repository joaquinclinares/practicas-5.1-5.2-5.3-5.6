<?php
class EContacte extends Contacte{

    private $email;
    private $pagina;
    public function __construct($datos){
        parent::__construct($datos);
        $this->email=$datos['email'];
        $this->pagina=$datos['pagina'];
    }
    public function __toString(){
        return parent::__toString().'</br>'.$this->email.', '. $this->pagina;
    }
    public function __clone(){
        self::incrementaCont();
        $this->id=self::$contador;
    }
    public function getEmail(){
        return $this->email;
    }
    public function setEmail($email){
        $this->email = $email;
        return $this;
    }
    public function getPagina(){
        return $this->pagina;
    }
    public function setPagina($pagina){
        $this->pagina = $pagina;
        return $this;
    }
}
?>